#include <jni.h>   
#include "src_GeneratedController.h"  
#include "controller.h"
#include "rtwtypes.h"  

JNIEXPORT float JNICALL Java_src_GeneratedController_generatedStep(JNIEnv *env, jobject obj, jfloat e) {
    rtU.In1 = e;
    controller_step();
    return rtY.Out1;
} 