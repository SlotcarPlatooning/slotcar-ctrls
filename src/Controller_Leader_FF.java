package src;

import java.time.ZonedDateTime;
import java.util.List;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.OutputType;


@ClassInfo(label = "Leader vel. follower", description = "The output of the controller is the leader's velocity. The car only follows leader's movement. No distance is used.", author="IH", version="0")
public class Controller_Leader_FF extends Controller {

	public float min = -1.5f;
	public float max = 1.5f;
	
	private static final float Ts = 0.03f;
	private static final long period = (long) (Ts * 1000.0f);

	public Controller_Leader_FF() {
		super(OutputType.SPEED);

		init();
	}

	@Override
	public long getMiliSecPeriod() {
		return period;
	}

	@Override
	public void afterWritingConstants() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
	}

	@Override
	public float step(CarImageInterface me, CarListInterface cars) {
		final float IGNORE_ERROR = 0.00f;
		final float IGNORE_INPUT = 0.00f;
		
		
		float eff = 0;
		try {
			CarImageInterface pre = cars.getByPos(0);
			float speedPre = pre.getCarMeasurement().getSpeed();
			eff = speedPre;
		} catch (Exception ex) {
			System.out.println("neprovedlo se");
		}
		
		float u = Nonlinearities.saturation(eff, min, max);
		
		// System.out.println(u);
		// float u = Integrator.saturation(e * this.kp,min,max);
		if ((u < IGNORE_INPUT) && (u > -IGNORE_INPUT)) {
			u = 0;
		}
		// System.out.println("u des" + calcEff + ", u sat: " + u + ", e: " + e
		// + ", dist ref: " + reference + ", meas dist: " + measurement);
		ZonedDateTime dateTime = ZonedDateTime.now();
		System.out.println("Time: " + dateTime.getSecond() + "." + (dateTime.getNano() / 1E9f) + ", Desired speed: " + u
				+ ", velocity is: " + me.getCarMeasurement().getSpeed());

		return u;
	}
}
