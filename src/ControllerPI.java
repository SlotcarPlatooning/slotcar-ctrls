package src;


import java.time.ZonedDateTime;
import java.util.List;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.FieldInfo;
import slotcar.pi.controller.OutputType;
@ClassInfo(label = "Distance PI Ctrl", description = "PI controller for distance for predecessor following control law, output is desired speed", author="IH", version="0")
public class ControllerPI extends Controller {

	@FieldInfo(label="P constant", description="Proportional constant of the controller")
	public float kp = 3.0f;
	@FieldInfo(label="I constant", description="Integration constant of the controller")
	public float ki = 0.8f;
	@FieldInfo(label="Min desired velocity [m/s]", description="Minimal reference velocity for the speed controller = limit of the conroller output")
	public float min = -1.5f;
	@FieldInfo(label="Max. desired velocity [m/s]", description="Maximal reference velocity for the speed controller = limit of the conroller output")
	public float max = 1.5f;
	@FieldInfo(label="Control error deadzone [m]", description="If either the front or rear control error is in +- this range, the error is set to zero.")
	public float IGNORE_ERROR = 0.01f;
	@FieldInfo(label="Desired vel. deadzone [m/s]", description="If the controller output (des. vel) is in +- this range, the desired velocity is set zero.")
	public float IGNORE_INPUT = 0.01f;
	private Integrator integrator;
	@FieldInfo(label="Sampling period [s]", description="Sampling period of the controller in [s].")
	public float Ts = 0.03f;
	private long period = (long) (Ts * 1000.0f);

	public ControllerPI() {
		super(OutputType.SPEED);
		init();
	}

	@Override
	public long getMiliSecPeriod() {
		return period;
	}

	@Override
	public void afterWritingConstants() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		integrator = new Integrator(this.min, this.max);
	}

	@Override
	public float step(CarImageInterface me, CarListInterface cars) {
		final float IGNORE_ERROR = 0.00f;
		final float IGNORE_INPUT = 0.00f;
		float reference = me.getCarControl().getReferenceDistance();
		float measurement = me.getCarMeasurement().getDistanceFront();

		float e = -(reference - measurement);
		if ((e < IGNORE_ERROR) && (e > -IGNORE_ERROR)) {
			e = 0;
		}
		// switch off integrator when P will take maximal effort
		if (Math.abs(e) < (this.max / this.kp)) {
			integrator.add(e * ki * Ts);
		} else {
			integrator.setValue(0);
		}

		float calcEff = e * this.kp + this.integrator.getValue();
		float u = Nonlinearities.saturation(calcEff, min, max);
		// System.out.println(u);
		// float u = Integrator.saturation(e * this.kp,min,max);
		if ((u < IGNORE_INPUT) && (u > -IGNORE_INPUT)) {
			u = 0;
		}
		// System.out.println("u des" + calcEff + ", u sat: " + u + ", e: " + e
		// + ", dist ref: " + reference + ", meas dist: " + measurement);
		ZonedDateTime dateTime = ZonedDateTime.now();
		System.out.println("Time: " + dateTime.getSecond() + "." + (dateTime.getNano() / 1E9f) + ", Desired speed: " + u
				+ ", velocity is: " + me.getCarMeasurement().getSpeed());

		return u;
	}
}
