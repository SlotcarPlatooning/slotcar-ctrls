/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;

import java.util.List;
import java.*;
import java.util.ArrayList;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.FieldInfo;
import slotcar.pi.controller.OutputType;

/**
 *
 * @author Michal Stanek
 */
public class DynamicController {

    // @FieldInfo(label = "Sampling period [s]", description = "Sampling period of the controller in [s].")
    // public float Ts = 0.03f;
    // private long period = (long) (Ts * 1000.0f);
    private ArrayList num = new ArrayList();
    private ArrayList den = new ArrayList();
    private float[][] A;
    private float[] B;
    private float[] C;
    private float D;
    private float[] state;

    public DynamicController(String num, String den) {

        String[] numString = num.split("[-+.,:;]");
        String[] denString = den.split("[-+.,:;]");
        for (String s : numString) {
            this.num.add(Float.parseFloat(s));
        }
        for (String s : denString) {
            this.den.add(Float.parseFloat(s));
        }
        toEquations(this.num, this.den);
        initializeState(this.num.size());
    }

    public float step(float input) {
        float output = 0;
        float pom = 0;

        float[] nextState = new float[this.state.length];

        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A.length; j++) {
                pom = pom + A[i][j] * this.state[j];
            }
            nextState[i] = pom + B[i] * input;
            pom = 0;
        }
        for (int i = 0; i < state.length; i++) {
            output = output + C[i] * state[i];
        }
        output = output  + D * input;
        
        this.state = nextState;
        return output;
    }

    public float[] getState() {
        return this.state;
    }

    private void initializeState(int size) {
        float[] state = new float[size];
        for (int j = 0; j < size - 1; j++) {
            state[j] = 0;
        }
        this.state = state;
    }

    private void toEquations(ArrayList<Float> num, ArrayList<Float> den) {
        float[][] a = new float[den.size() - 1][den.size() - 1];
        for (int i = 0; i < den.size() - 1; i++) {
            for (int j = 0; j < den.size() - 1; j++) {
                if (i + 1 == j) {
                    a[i][j] = 1;
                } else {
                    a[i][j] = 0;
                }
            }
        }
        for (int j = 0; j < den.size() - 1; j++) {
            a[den.size() - 2][j] = -den.get(den.size() - 1 - j) / den.get(0);
        }
        float[] b = new float[den.size() - 1];
        for (int j = 0; j < den.size() - 1; j++) {
            b[j] = 0;
        }
        b[den.size() - 2] = 1;

        float[] c = new float[den.size() - 1];
        for (int j = 0; j < den.size() - 1; j++) {
            c[j] = 0;
        }
        for (int j = 0; j < num.size(); j++) {
            c[j] = num.get(num.size() - 1 - j) / den.get(0);
        }
        float d = 0;

        this.A = a;
        this.B = b;
        this.C = c;
        this.D = d;
    }

}
