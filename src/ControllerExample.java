package src;


import java.util.List;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.FieldInfo;
import slotcar.pi.controller.OutputType;

@ClassInfo(label = "Speed Controller", description = "Example of a speed controller, using integrated speed controller, so it does nothing", author="none", version="0")
public class ControllerExample extends Controller {
	
	@FieldInfo(label="output multiplication", description="")
	public float mult = 1.0f;

	public ControllerExample() {
		super(OutputType.SPEED);
	}
	
	@Override
	public long getMiliSecPeriod() {
		return 100;
	}

	@Override
	public void afterWritingConstants() {
		// do nothing
	}

	@Override
	public void init() {
		// do nothing
	}

	@Override
	public float step(CarImageInterface me, CarListInterface cars) {
		float u = mult * me.getCarControl().getReferenceSpeed();
		System.out.println(u);
		return u;
	}

}
