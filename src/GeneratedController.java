/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;


class GeneratedController {

    private native float generatedStep(float e);

    public static void main(String[] args) {
        float u = new GeneratedController().generatedStep(5);
        System.out.println(u);
    }

    static {
    	try{
            //System.load("/slotcar/library/Controller.so");
            System.loadLibrary("Controller");
    	}
    	catch(UnsatisfiedLinkError e){
    		System.out.println("Can't load Controller.so, probably because the GUI runs on Windows. This is not an error.");
    	}
    	finally{
    		
    	}
    }
}
