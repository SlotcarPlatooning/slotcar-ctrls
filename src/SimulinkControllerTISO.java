package src;


import java.time.ZonedDateTime;
import java.util.List;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.FieldInfo;
import slotcar.pi.controller.OutputType;

@ClassInfo(label = "TISO Simulink Ctrl", description = "TODO", author = "MS", version = "0")

/** 
 * The SimulinkControllerTISO class implements a two-input single-output controller wrapper that
 * extends Controller interface. It declares native method generatedStep(), which is implemented in the shared library. 
 * Shared library is generated from Simulink scheme and loaded statically. 
 * Method step() is given two arguments and returns controller's output calculated in a single period sample.
 * 
 * @author Michal Stanek
 *   
 */
public class SimulinkControllerTISO extends Controller {

	//@FieldInfo(label = "P constant", description = "Proportional constant of the controller")
    //public float kp = 3.0f;
    //@FieldInfo(label = "I constant", description = "Integration constant of the controller")
    //public float ki = 0.8f;
    @FieldInfo(label = "Min desired velocity [m/s]", description = "Minimal reference velocity for the speed controller = limit of the conroller output")
    public float min = -1.5f;
    @FieldInfo(label = "Max. desired velocity [m/s]", description = "Maximal reference velocity for the speed controller = limit of the conroller output")
    public float max = 1.5f;
    @FieldInfo(label = "Control error deadzone [m]", description = "If either the front or rear control error is in +- this range, the error is set to zero.")
    public float IGNORE_ERROR = 0.01f;
    @FieldInfo(label = "Desired vel. deadzone [m/s]", description = "If the controller output (des. vel) is in +- this range, the desired velocity is set zero.")
    public float IGNORE_INPUT = 0.01f;
    private Integrator integrator;
    @FieldInfo(label = "Sampling period [s]", description = "Sampling period of the controller in [s].")
    public float Ts = 0.03f;
    private long period = (long) (Ts * 1000.0f);

    
    /**
     * Declaration of the native method. Implementation is in the shared library loaded statically.
     * 
     * @param e is the first given input to the controller, float number is needed
     * @param f is the second given input to the controller, float number is needed
     * @return output of the controller
     */
    private native float generatedStep(float e, float f);
    
    /**
     * Tries to load shared library, which is saved in the library folder in a slotcar.
     * Library contains C code generated from the Simulink scheme of the controller and 
     * Java native interface needed to implement generated C code into Java project.
     */ 
    static {

    	try{
            System.load("/slotcar/library/TISOController.so");
    	}
    	catch(UnsatisfiedLinkError e){
    		System.out.println("Can't load TISOController.so, file does not exist.");
    	}
    	finally{
    		
    	}
    }
    
	public SimulinkControllerTISO() {
		super(OutputType.SPEED);
		init();
	}

	@Override
	public void afterWritingConstants() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public long getMiliSecPeriod() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Method is a wrapper for the generatedStep() method. Prepares inputs for the controller and calls the generatedStep() method.
	 * All nonlinearities are handled in the generated method, see the Simulink scheme for more details.
	 * @param me represents an instance of the controlled slotcar.
     * @param cars represents a list of instances of slotcars in the controlled system.
     * @return output of the controller in a single period sample
	 */
	@Override
	public float step(CarImageInterface me, CarListInterface cars) {
		final float IGNORE_ERROR = 0.00f;
        final float IGNORE_INPUT = 0.00f;
        float reference = me.getCarControl().getReferenceDistance();
        float measurement = me.getCarMeasurement().getDistanceFront();

        float e = -(reference - measurement);
        if ((e < IGNORE_ERROR) && (e > -IGNORE_ERROR)) {
            e = 0;
        }
        
        float u = generatedStep(e, 5);
        //float u = Nonlinearities.saturation(calcEff, min, max);
        
        // System.out.println(u);
        // float u = Integrator.saturation(e * this.kp,min,max);
        if ((u < IGNORE_INPUT) && (u > -IGNORE_INPUT)) {
            u = 0;
        }
        // System.out.println("u des" + calcEff + ", u sat: " + u + ", e: " + e
        // + ", dist ref: " + reference + ", meas dist: " + measurement);
        ZonedDateTime dateTime = ZonedDateTime.now();
        System.out.println("Time: " + dateTime.getSecond() + "." + (dateTime.getNano() / 1E9f) + ", Desired speed: " + u
                + ", velocity is: " + me.getCarMeasurement().getSpeed());

        return u;
	}

}
