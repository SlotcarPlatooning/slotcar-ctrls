package src;

import java.time.ZonedDateTime;
import java.util.List;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.FieldInfo;
import slotcar.pi.controller.OutputType;


@ClassInfo(label = "Distance PD Ctrl-FF from ldr", description = "PD controller for distance with feedforward from leader's velocity, output is desired speed", author="IH", version="0")
public class ControllerPD_FF_Leader extends Controller {

	@FieldInfo(label="P constant", description="P constant of the controller")
	public float kp = 5f;
	@FieldInfo(label="D constant", description="D constant of the controller")
	public float kd = 0.2f;
	@FieldInfo(label="Nfilt constant", description="Filter constant of the controller")
	public float Nfilt = 50.0f;
	@FieldInfo(label="Min reference velocity", description="Minimal reference velocity for the speed controller = limit of the conroller output")
	public float min = -1.5f;
	@FieldInfo(label="Max. reference velocity", description="Maximal reference velocity for the speed controller = limit of the conroller output")
	public float max = 1.5f;
	@FieldInfo(label="Control error deadzone [m]", description="If either the front or rear control error is in +- this range, the error is set to zero.")
	public float IGNORE_ERROR = 0.01f;
	@FieldInfo(label="Desired vel. deadzone [m/s]", description="If the controller output (des. vel) is in +- this range, the desired velocity is set zero.")
	public float IGNORE_INPUT = 0.01f;
	
	private float PDstate = 0.0f;
	private float prev_e = 0.0f;
	private static final float Ts = 0.03f;
	private static final long period = (long) (Ts * 1000.0f);

	public ControllerPD_FF_Leader() {
		super(OutputType.SPEED);
		PDstate = 0.0f;
		prev_e = 0.0f;
		init();
	}

	@Override
	public long getMiliSecPeriod() {
		return period;
	}

	@Override
	public void afterWritingConstants() {
		// TODO Auto-generated method stub
		PDstate = 0.0f;
		prev_e = 0.0f;
	}

	@Override
	public void init() {
		PDstate = 0.0f;
		prev_e = 0.0f;
	}

	@Override
	public float step(CarImageInterface me, CarListInterface cars) {

		float reference = me.getCarControl().getReferenceDistance();
		float measurement = me.getCarMeasurement().getDistanceFront();

		float e = -(reference - measurement);
		if ((e < IGNORE_ERROR) && (e > -IGNORE_ERROR)) {
			e = 0;
		}
		

		
		float calcEff = -(Nfilt*Ts-1)*PDstate + (kp+kd*Nfilt)*e + (kp*Ts*Nfilt - kp - kd*Nfilt)*prev_e;
		
		PDstate = calcEff;
		prev_e = e;
		
		float eff = calcEff;
		try {
			CarImageInterface pre = cars.getByPos(0);
			float speedPre = pre.getCarMeasurement().getSpeed();
			eff += speedPre;
		} catch (Exception ex) {
			System.out.println("neprovedlo se");
		}
		
		float u = Nonlinearities.saturation(eff, min, max);
		
		// System.out.println(u);
		// float u = Integrator.saturation(e * this.kp,min,max);
		if ((u < IGNORE_INPUT) && (u > -IGNORE_INPUT)) {
			u = 0;
		}
		// System.out.println("u des" + calcEff + ", u sat: " + u + ", e: " + e
		// + ", dist ref: " + reference + ", meas dist: " + measurement);
		ZonedDateTime dateTime = ZonedDateTime.now();
		System.out.println("Time: " + dateTime.getSecond() + "." + (dateTime.getNano() / 1E9f) + ", Desired speed: " + u
				+ ", velocity is: " + me.getCarMeasurement().getSpeed());

		return u;
	}
}
