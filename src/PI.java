package src;


public class PI {

	Integrator integrator;
	float min, max;

	public PI(float min, float max, float inputError, float outputError) {
		this.min = min;
		this.max = max;
		integrator = new Integrator(min, max);

	}

	float step(float reference, float measurement, float kp, float ki, float ts) {
		float e = reference - measurement;
		// switch off integrator when P will take maximal effort
		if (Math.abs(e) < (this.max / kp)) {
			integrator.add(e * ki * ts);
		} else {
			integrator.setValue(0);
		}
		float u = kp * e + integrator.getValue();
		u = Nonlinearities.saturation(u, min, max);

		// u = deadzoneOut.apply();
		return u;
	}

	public static void main(String[] args) {
		PI pi = new PI(-1.0f, 1.0f, 0f, 0f);
		float u = pi.step(0.22f, 0.28f, 5.0f, 2.0f, 0.03f);

		for (int i = 0; i < 200; i++) {
			System.out.println(pi.step(-0.22f, -0.28f, 5.0f, 2.0f, 0.03f));
		}
	}
}
