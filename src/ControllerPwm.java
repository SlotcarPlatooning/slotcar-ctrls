package src;


import java.util.List;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.FieldInfo;
import slotcar.pi.controller.OutputType;

@ClassInfo(label = "Pwm Controller", description = "Example of a PWM controller, using integrated PWM controller, so it does nothing", author="none", version="0")
public class ControllerPwm extends Controller {
	
	@FieldInfo(label="output multiplication", description="")
	public float mult = 1.0f;

	public ControllerPwm() {
		super(OutputType.PWM);
	}
	
	@Override
	public long getMiliSecPeriod() {
		return 100;
	}

	@Override
	public void afterWritingConstants() {
		// do nothing
	}

	@Override
	public void init() {
		// do nothing
	}

	@Override
	public float step(CarImageInterface me, CarListInterface cars) {
		float u = mult * me.getCarControl().getReferencePwm();
		System.out.println(u);
		return u;
	}

}
