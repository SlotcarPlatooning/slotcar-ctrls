package src;

import java.time.ZonedDateTime;
import java.util.List;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.FieldInfo;
import slotcar.pi.controller.OutputType;

@ClassInfo(label = "Distance PI Ctrl-Bidirectional", description = "PI controller for distance for bidirectional control law, output is desired speed", author="IH", version="0")
public class ControllerPI_Bid extends Controller {

	@FieldInfo(label="P constant", description="Proportional constant of the controller")
	public float kp = 3.0f;
	@FieldInfo(label="I constant", description="Integration constant of the controller")
	public float ki = 0.8f;
	@FieldInfo(label="Espilon", description="Weight of the rear spacing error. 0 means do not use rear error, 1 means symmetric control")
	public float epsilon = 1.0f; 
	@FieldInfo(label="Min desired velocity [m/s]", description="Minimal reference velocity for the speed controller = limit of the conroller output")
	public float min = -1.5f;
	@FieldInfo(label="Max. desired velocity [m/s]", description="Maximal reference velocity for the speed controller = limit of the conroller output")
	public float max = 1.5f;
	@FieldInfo(label="Control error deadzone [m]", description="If either the front or rear control error is in +- this range, the error is set to zero.")
	public float IGNORE_ERROR = 0.01f;
	@FieldInfo(label="Desired vel. deadzone [m/s]", description="If the controller output (des. vel) is in +- this range, the desired velocity is set zero.")
	public float IGNORE_INPUT = 0.01f;
	private Integrator integratorf;
	private Integrator integratorb;
	@FieldInfo(label="Sampling period [s]", description="Sampling period of the controller in [s].")
	public float Ts = 0.03f;
	private long period = (long) (Ts * 1000.0f);

	public ControllerPI_Bid() {
		super(OutputType.SPEED);
		init();
	}

	@Override
	public long getMiliSecPeriod() {
		return period;
	}

	@Override
	public void afterWritingConstants() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		integratorf = new Integrator(this.min, this.max);
		integratorb = new Integrator(this.min, this.max);
	}

	@Override
	public float step(CarImageInterface me, CarListInterface cars) {
		float reference = me.getCarControl().getReferenceDistance();
		float measurementf = me.getCarMeasurement().getDistanceFront();
		float measurementb = me.getCarMeasurement().getDistanceBack();
		
		float wb = epsilon;
		if (cars.getMyPos() >= cars.size()-1) 
		{
			//last car in the platoon
			wb = 0.0f;
		}
		
		float ef = -(reference - measurementf);
		if ((ef < IGNORE_ERROR) && (ef > -IGNORE_ERROR)) {
			ef = 0.0f;
		}
		
		float eb = -(reference - measurementb);
		if ((eb < IGNORE_ERROR) && (eb > -IGNORE_ERROR)) {
			eb = 0.0f;
		}
		System.out.println("Position" + cars.getMyPos());
		System.out.println(reference + "mb" + measurementb + "mf" + measurementf );
		
		
		// switch off integrator when P will take maximal effort
		if (Math.abs(ef) < (this.max / this.kp)) {
			integratorf.add(ef * ki * Ts);
		} else {
			integratorf.setValue(0.0f);
		}
		
		// switch off integrator when P will take maximal effort

		if (Math.abs(eb) < (this.max / this.kp)) {
			integratorb.add(eb * ki * Ts);
		} else {
			integratorb.setValue(0.0f);
		}
		System.out.println(epsilon +" " + this.kp +" "+ this.ki+" " + this.Ts + "\n"+"Errors back" + eb+ "intb" + integratorb.getValue() +" front" + ef + "intf" + integratorf.getValue());
		


		float calcEff = ef * this.kp + this.integratorf.getValue() - wb*( eb * this.kp + this.integratorb.getValue());
		float u = Nonlinearities.saturation(calcEff, min, max);
		// System.out.println(u);
		// float u = Integrator.saturation(e * this.kp,min,max);
		if ((u < IGNORE_INPUT) && (u > -IGNORE_INPUT)) {
			u = 0.0f;
		}
		// System.out.println("u des" + calcEff + ", u sat: " + u + ", e: " + e
		// + ", dist ref: " + reference + ", meas dist: " + measurement);
		ZonedDateTime dateTime = ZonedDateTime.now();
		System.out.println("Time: " + dateTime.getSecond() + "." + (dateTime.getNano() / 1E9f) + ", Desired speed: " + u + ", velocity is: " + me.getCarMeasurement().getSpeed());

		return u;
	}
}
