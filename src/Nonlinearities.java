package src;

public class Nonlinearities {

	public static float saturation(float u, float min, float max) {
		return Math.min(Math.max(u, min), max);
	}

	public static float deadzoneTypeOne(float u, float min, float max) {
		return (u >= min && u <= max) ? 0 : u;
	}

	public static float deadzoneTypeTwo(float u, float min, float max) {
		return (u >= min && u <= max) ? 0 : (u > max) ? u - max : u - min;
	}

}
