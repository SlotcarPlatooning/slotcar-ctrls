package src;



import java.time.ZonedDateTime;
import java.util.List;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.OutputType;


@ClassInfo(label = "Distance Controller", description = "controller for distance, output is directly PWM", author="IH", version="0")
public class ControllerPI_PWM extends Controller {

	public float kp = 2.5f;
	public float ki = 1.0f;
	public float min = -1.0f;
	public float max = 1.0f;

	private Integrator integrator;
	private static final float Ts = 0.03f;
	private static final long period = (long) (Ts * 1000.0f);

	public ControllerPI_PWM() {
		super(OutputType.PWM);
		init();
	}

	@Override
	public long getMiliSecPeriod() {
		return period;
	}

	@Override
	public void afterWritingConstants() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		integrator = new Integrator(this.min, this.max);
	}

	@Override
	public float step(CarImageInterface me, CarListInterface cars) {
		final float IGNORE_ERROR = 0.00f;
		final float IGNORE_INPUT = 0.00f;
		float reference = me.getCarControl().getReferenceDistance();
		float measurement = me.getCarMeasurement().getDistanceFront();

		float e = -(reference - measurement);
		if ((e < IGNORE_ERROR) && (e > -IGNORE_ERROR)) {
			e = 0;
		}
		// switch off integrator when P will take maximal effort
		if (Math.abs(e) < (this.max / this.kp)) {
			integrator.add(e * ki * Ts);
		} else {
			integrator.setValue(0);
		}

		float calcEff = e * this.kp + this.integrator.getValue();
		float u = Nonlinearities.saturation(calcEff, min, max);
		// System.out.println(u);
		// float u = Integrator.saturation(e * this.kp,min,max);
		if ((u < IGNORE_INPUT) && (u > -IGNORE_INPUT)) {
			u = 0;
		}
		// System.out.println("u des" + calcEff + ", u sat: " + u + ", e: " + e
		// + ", dist ref: " + reference + ", meas dist: " + measurement);
		ZonedDateTime dateTime = ZonedDateTime.now();
		System.out.println("Time: " + dateTime.getSecond() + "." + (dateTime.getNano() / 1E9f) + ", Desired speed: " + u
				+ ", velocity is: " + me.getCarMeasurement().getSpeed());

		return u;
	}
}
