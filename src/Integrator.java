package src;


public class Integrator {
	private float value;
	
	private float min,max;
	private boolean saturation = false;
	
	public Integrator(float min, float max) {
		this.min = min;
		this.max = max;
		saturation = true;
	}

	public Integrator() {
		
	}
	
	public void add(float increment) {
		setValue(getValue() + increment); 
	}

	public float getValue() {
		return value;
	}

	public void setValue(float val) {
		value = (this.saturation) ? Nonlinearities.saturation(val,min,max) : val;
	}
}
