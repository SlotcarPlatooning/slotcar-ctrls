package src;

import java.time.ZonedDateTime;
import java.util.List;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.OutputType;


@ClassInfo(label = "Distance PD Controller", description = "PD controller for distance, output is desired speed", author="IH", version="0")
public class ControllerPD extends Controller {

	public float kp = 5f;
	public float kd = 0.2f;
	public float Nfilt = 50.0f;
	public float min = -1.5f;
	public float max = 1.5f;
	
	private float PDstate = 0.0f;
	private float prev_e = 0.0f;
	private static final float Ts = 0.03f;
	private static final long period = (long) (Ts * 1000.0f);

	public ControllerPD() {
		super(OutputType.SPEED);
		PDstate = 0.0f;
		prev_e = 0.0f;
		init();
	}

	@Override
	public long getMiliSecPeriod() {
		return period;
	}

	@Override
	public void afterWritingConstants() {
		// TODO Auto-generated method stub
		PDstate = 0.0f;
		prev_e = 0.0f;
	}

	@Override
	public void init() {
		PDstate = 0.0f;
		prev_e = 0.0f;
	}

	@Override
	public float step(CarImageInterface me, CarListInterface cars) {
		final float IGNORE_ERROR = 0.00f;
		final float IGNORE_INPUT = 0.00f;
		float reference = me.getCarControl().getReferenceDistance();
		float measurement = me.getCarMeasurement().getDistanceFront();

		float e = -(reference - measurement);
		if ((e < IGNORE_ERROR) && (e > -IGNORE_ERROR)) {
			e = 0;
		}
		

		float calcEff = -(Nfilt*Ts-1)*PDstate + (kp+kd*Nfilt)*e + (kp*Ts*Nfilt - kp - kd*Nfilt)*prev_e;
		
		PDstate = calcEff;
		prev_e = e;
		float u = Nonlinearities.saturation(calcEff, min, max);
		// System.out.println(u);
		// float u = Integrator.saturation(e * this.kp,min,max);
		if ((u < IGNORE_INPUT) && (u > -IGNORE_INPUT)) {
			u = 0;
		}
		// System.out.println("u des" + calcEff + ", u sat: " + u + ", e: " + e
		// + ", dist ref: " + reference + ", meas dist: " + measurement);
		ZonedDateTime dateTime = ZonedDateTime.now();
		System.out.println("Time: " + dateTime.getSecond() + "." + (dateTime.getNano() / 1E9f) + ", Desired speed: " + u
				+ ", velocity is: " + me.getCarMeasurement().getSpeed());

		return u;
	}
}
