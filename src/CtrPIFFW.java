package src;


import java.time.ZonedDateTime;
import java.util.List;

import javax.swing.event.CaretListener;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.OutputType;

public class CtrPIFFW extends Controller {
	Integrator i;

	private static final float MIN = -1.5f;
	private static final float MAX = 1.5f;
	private static final float IGNORE_ERROR = 0.00f;
	private static final float IGNORE_INPUT = 0.00f;

	public float ki = 1.0f;
	public float kp = 5.0f;

	private static final float Ts = 0.03f;
	private static final long period = (long) (Ts * 1000.0f);

	private PI pi;

	public CtrPIFFW() {
		super(OutputType.SPEED);
		init();
	}

	@Override
	public void afterWritingConstants() {
		// TODO Auto-generated method stub

	}

	@Override
	public long getMiliSecPeriod() {
		return period;
	}

	@Override
	public void init() {
		i = new Integrator(MIN, MAX);
		pi = new PI(MIN, MAX, IGNORE_ERROR, IGNORE_INPUT);
	}

	@Override
	public float step(CarImageInterface me, CarListInterface cars) {

		float reference = me.getCarControl().getReferenceDistance();
		float measurement = me.getCarMeasurement().getDistanceFront();

		float u = pi.step(-reference, -measurement, kp, ki, Ts);

		float speedPre = -Float.MIN_VALUE;
		
		try {
			CarImageInterface pre = cars.getByPos(0);
			speedPre = pre.getCarMeasurement().getSpeed();
			u += speedPre;
			Nonlinearities.saturation(u, MIN, MAX);
		} catch (Exception ex) {
			System.out.println("neprovedlo se");
		}

		ZonedDateTime dateTime = ZonedDateTime.now();
		String s1 = String.format("Time %d.%f\n",dateTime.getSecond(), (float) (dateTime.getNano() / 1E9f));
		String s2 = String.format("Distance: %f / %f\n",me.getCarMeasurement().getDistanceFront(),me.getCarControl().getReferenceDistance());
		String s3 = String.format("Speed %f / %f\n", u,speedPre);
		StringBuilder s = new StringBuilder();
		s.append(s1).append(s2).append(s3);
		System.out.print(s.toString());
		return u;

	}

}
