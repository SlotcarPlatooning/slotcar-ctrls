#include <jni.h>   
#include "src_ControllerPI_generated.h"  
#include "controller.h"
#include "rtwtypes.h"  

JNIEXPORT float JNICALL Java_src_ControllerPI_generated_generatedStep(JNIEnv *env, jobject obj, jfloat e) {
    rtU.In1 = e;
    controller_step();
    return rtY.Out1;
}